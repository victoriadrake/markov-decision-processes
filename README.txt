Markov Decision Processes

This repository contains Jupyter notebooks for exploring Markov Decision Processes (MDPs) with the Forest Management and Frozen Lake examples. Contents:

- Implementation of Value Iteration and Policy Iteration on:
    - Forest Management: forest.ipynb
    - Frozen Lake: frozenlake.ipynb
- Implementation of Q-Learning on both: q-learning.ipynb

To run the notebooks:

1. Ensure Jupyter is installed. See: https://jupyter.org/install
2. Install dependencies from requirements.txt or the Pipfile using your choice of Python environment
3. Launch Jupyter with the terminal command: jupyter notebook
